<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class HelpersTest extends TestCase
{
    /** @test */
    public function page_title_should_return_the_base_titleif_title_is_empty()
    {	
        $this->assertEquals('Laracarte - List of Artisans',page_title(''));
    }
    /** @test */
    public function page_title_should_return_the_correct_title_if_title_is_provide()
    {	
        $this->assertEquals('About|Laracarte - List of Artisans',page_title('About'));
       	$this->assertEquals('Home|Laracarte - List of Artisans',page_title('Home'));
    }
/** @test */
    public function set_active_root_should_return_the_correct_class_based_on_a_given_root()
    {	
    	$this->get(route('about_path'));
        $this->assertEquals('active',set_active_root('about_path'));
        $this->assertEquals('',set_active_root('contacts'));
       	
       	$this->get(route('contact_path'));
        $this->assertEquals('active',set_active_root('contact_path'));
        $this->assertEquals('',set_active_root('contacts'));
        $this->assertEquals('',set_active_root('root_path'));
    }

}
