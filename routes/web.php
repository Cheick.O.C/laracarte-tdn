<?php

use App\Http\Controllers\contactsController;
use App\Http\Controllers\pagesController;
use App\Mail\ContactMessageCreated;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'pages.home')->name('root_path');
Route::view('/about', 'pages.about')->name('about_path');

Route::get('/contact', [ contactsController::class, 'create'])->name('contact_path');

Route::post('/contact', [ contactsController::class, 'store'])->name('contact_path');
