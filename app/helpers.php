<?php 

if(! function_exists('page_title')){
	function page_title($title){
		$base_title = config('app.name').' - List of Artisans';
		return empty($title) ? $base_title : sprintf('%s|%s',$title,$base_title);
		
	}
}

if(! function_exists('set_active_root')){
	function set_active_root($root){
		return Route::is($root) ? 'active' : '';
	}
}