<?php  
namespace App\Scope;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait DateScopable{

public function scopeTwoMonthOld(Builder $query){
    	$query->where('created_at', '<=', now()->subMonths('2'));
    }

}