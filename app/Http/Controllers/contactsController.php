<?php

namespace App\Http\Controllers;

use App\Http\Requests\contactFormRequest;
use App\Mail\ContactMessageCreated;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class contactsController extends Controller
{
    public function create(){
    	return view('contacts.create');
    }

    public function store(contactFormRequest $request){

    	$message = Message::create($request->only('name','email','message'));

    	Mail::to(config('Laracarte.sender_email'))->send(new ContactMessageCreated($message) );

    	flashy('Nous vous repondrons dans un bref délais');	

    	return redirect()->route('root_path');
    }
}
