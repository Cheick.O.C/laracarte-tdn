@extends('layouts.default',['title'=>'About'])

@section('content')
    <div class="container">
    	<h2>What's {{ config('app.name') }}</h2>
    	<p>{{ config('app.name') }} is the clone of app <a href="https://laramap.com" target="_blank">Laramap.com</a></p>
    	<div class="row">
    		<div class="col-md-6">
    			<div class="alert alert-warning">
    				<i class="fa fa-exclamation-triangle"></i>
    				<strong>this app as been built by <a href="https://twitter.com/">@etsmo</a> for learning purposes.</strong>
    			</div>
    		</div>
    	</div>

    	<p> feel free to help to improve the <a href="https://gitlab.com/Cheick.O.C/laracarte-tdn"> source code</a></p>
    	
    	<hr>

    	<h2>What's Laramap ?</h2>
    	<p>Laramap is the website by witch {{ config('app.name') }} was inspired:)</p>
    	<p>More infos <a href="http://laramap.com">here</a></p>

    	<hr>

    	<h2>Which tools and services are used in {{ config('app.name') }} ?</h2>
    	<p>
    	basically it's built on laravel &amp; bootstrap .  but there is a bunch of services used for email and other section
    	</p>	
    	<ul>
    		<li>Laravel</li>
    		<li>Bootstrap</li>
    		<li>Amazon S3</li>
    		<li>Mandrill</li>
    		<li>Google Maps</li>
    		<li>Gravatar</li>
    		<li>Antonie Martin's Geolocation packages</li>
    		<li>Michel Fortin's Markdown Parser Package</li>
    		<li>Heroku</li>
    	</ul>

    </div>
@endsection
