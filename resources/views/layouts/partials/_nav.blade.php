 <nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
  <div class="container">
      <a class="navbar-brand" href="{{ route('root_path') }}">{{ config('app.name') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link {{ set_active_root('root_path') }} " href="{{ route('root_path') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ set_active_root('about_path') }}" href="{{ route('about_path') }}">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="#" tabindex="-1" aria-disabled="true">Artisan</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Planète</a>
          <div class="dropdown-menu" aria-labelledby="dropdown01">
            
            <a class="dropdown-item" href="https://laravel.com">laravel.com</a>
            <a class="dropdown-item" href="https://laravel.io">laravel.io</a>
            <a class="dropdown-item" href="https://laracasts.com">Laracasts</a>
            <a class="dropdown-item" href="https://larajobs.com">Larajob</a>
            <a class="dropdown-item" href="https://laravel-new.com">Laravel New</a>
            <a class="dropdown-item" href="https://larachats.com">Larachats</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ set_active_root('contact_path') }}" href="{{ route('contact_path') }}" tabindex="-1" aria-disabled="true">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav navbar-right">
        <li class="nav-item"><a class="nav-link" href="">Login</a></li>
        <li class="nav-item"><a class="nav-link" href="">Register</a></li>
      </ul>
    </div>
  </div>
  
</nav>