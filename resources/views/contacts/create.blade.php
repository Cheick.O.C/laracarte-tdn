@extends('layouts.default',['title'=>'Contact'])

@section('content')
   <div class="container">
       <div class="row justify-content-md-center">
           <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
               <h2>Get in Touch</h2>
               <p class="text-muted">if you having trouble whith this services, please <a href="{{config('Laracarte.admin_support_email')}}">    ask for help</a> 
               </p>

               <form action="{{ route('contact_path') }}" method="POST">
                    @csrf
                   <div class="form-group">
                        <label for="name" class="control-label">Name</label>
                       <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" required value="{{ old('name') }}">
                       {!! $errors->first('name', '<span class="help-block invalid-feedback">:message</span>') !!}
                   </div>

                   <div class="form-group">
                        <label for="email" class="control-label">Email</label>
                       <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" required value="{{ old('email') }}">
                       {!! $errors->first('email', '<span class="help-block invalid-feedback">:message</span>') !!}
                   </div>

                   <div class="form-group">
                        <label for="message" class="control-label {{ $errors->has('message') ? 'is-invalid' : '' }} sr-only">Message</label>
                       <textarea name="message" id="message" class="form-control" required rows="10" cols="10">{{ old('message') }}</textarea>
                       {!! $errors->first('message', '<span class="help-block invalid-feedback">:message</span>') !!}
                   </div>
                   <button class="btn btn-secondary btn-block">submit enquiry &raquo;</button>
                   
               </form>
           </div>
       </div>
   </div>                   
@endsection
